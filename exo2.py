import random

personnes = []

for i in range(10):
    age = random.randint(7,65)
    sexe =random.choice(['Masculin','Féminin'])
    taille = random.uniform(1.21,2)
    poids = random.uniform(21,100)
    personnes.append([age, sexe, taille, poids])

    for personne in personnes :
        imc =personne[3] /(personne[2]**2)

        if personne[0] <= 20:
            print(f"{personne[1]} tu as {personne[0]} ans. Ton IMC ne peut être calculé.")
        else :
            print(f"{personne[1]} tu as {personne[0]} ans. Ton IMC est {imc}.")
